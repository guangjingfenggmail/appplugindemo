package com.open.plugin.config

class MethodVisitorBuildConfig {
    //MethodVisitor
    Closure methodVisitor
    String className
    String interfaceName
    String methodName
    String methodDesc
    //需要修改的类列表
    //例如宿主：MainActivity.class
    //三方包：com.xiaola.module_main.main.MainActivity
    List<String> include = new ArrayList<>()
}