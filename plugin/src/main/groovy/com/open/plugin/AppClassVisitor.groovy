package com.open.plugin

import com.open.plugin.config.MethodVisitorBuildConfig
import org.objectweb.asm.ClassVisitor
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes

class AppClassVisitor extends ClassVisitor implements Opcodes {
    private String mClassName;
    private String[] mInterfaces

    AppClassVisitor(final ClassVisitor classVisitor) {
        super(Opcodes.ASM6, classVisitor)
    }

    @Override
    void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        mClassName = name
        mInterfaces = interfaces
        super.visit(version, access, name, signature, superName, interfaces)
    }

    /**
     * app的module里头设置的自动埋点方法修改器
     *
     * @param className 类名
     * @param methodVisitor 需要修改的方法
     * @param name 方法名
     * @param desc 参数描述符
     */
    private static MethodVisitor getMethodVisitor(MethodVisitorBuildConfig filter,
                                                         MethodVisitor methodVisitor, int access, String name, String desc) {
        MethodVisitor adapter = null
        Closure closure = filter.methodVisitor
        if (closure != null) {
            try {
                adapter = closure(methodVisitor, access, name, desc)
            } catch (Exception e) {
                e.printStackTrace()
            }
        }
        return adapter
    }

    /**
     * 是否修改用户自定义方法
     * 匹配规则有3种：
     *  1、有注解的话全部匹配
     *  2、类名+方法名+方法签名匹配
     *  3、接口名+方法名+方法签名匹配
     *
     * @param filter 用户自定义对象
     * @param className 扫描到的类名
     * @param methodName 扫描到的方法名
     * @param methodDesc 扫描到的方法签名
     * @param interfaces 扫描到的接口数组
     *
     */
    static boolean isModifyMethod(MethodVisitorBuildConfig filter, String className, String methodName
                                              , String methodDesc, String[] interfaces) {
//        boolean isMatchClass = filter.className == className
        boolean isMatchMethod = filter.methodName == methodName
        boolean isMatchMethodDes = filter.methodDesc == methodDesc

//        boolean isMatchInteface
//        interfaces.each {
//            String inteface ->
//                if (filter.interfaceName == inteface)
//                    isMatchInteface = true
//        }
        // 前提:方法名和方法签名匹配
        // 2、类也得匹配
        // 3、接口也得匹配
        if (isMatchMethod && isMatchMethodDes) {
            return true
        } else {
            return false
        }

    }

    @Override
    MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
        MethodVisitor methodVisitor = super.visitMethod(access, name, desc, signature, exceptions)
        MethodVisitor adapter = null
        // 2、用户在build.gradle自定义的MethodVisitor
        List<MethodVisitorBuildConfig> autoClassFilter = AppClassModifier.methodVisitorList
        autoClassFilter.each {
            MethodVisitorBuildConfig filter ->
                System.out.println("AppPlugin : isShouldModifyCustomMethod mClassName == " + mClassName + "; name ----> " + name+";desc == "+desc)
                if (isModifyMethod(filter, mClassName, name, desc, mInterfaces)) {
                    MethodVisitor userMethodVisitor
                    if (adapter == null) {
                        userMethodVisitor = getMethodVisitor(filter, methodVisitor, access, name, desc)
                    } else {
                        userMethodVisitor = getMethodVisitor(filter, adapter, access, name, desc)
                    }
                    adapter = userMethodVisitor
                }
        }
        if (adapter != null) {
            return adapter
        }
        return methodVisitor
    }
}