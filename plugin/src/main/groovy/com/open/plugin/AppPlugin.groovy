package com.open.plugin

import com.open.plugin.config.CustomExtension
import com.open.plugin.config.MethodVisitorBuildConfig
import org.gradle.api.Plugin
import org.gradle.api.Project

class AppPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        CustomExtension extension = project.extensions.create("customExtension", CustomExtension)
        com.android.build.gradle.AppExtension appExtension = project.extensions.findByType(com.android.build.gradle.AppExtension.class)
        appExtension.registerTransform(new AppTransform(project, extension,"1"))
        appExtension.registerTransform(new AppTransform(project, extension,"2"))

        project.afterEvaluate {
            customConfig(project)
        }
    }

    static void customConfig(Project project){
        AppClassModifier.methodVisitorList.clear()
        AppClassModifier.include.clear()

        //解析节点
        List<MethodVisitorBuildConfig> configArrayList = new ArrayList<>()
        List<Map<String, Object>> dataList = project.customExtension.customData

        dataList.each {
            Map<String, Object> map ->
                MethodVisitorBuildConfig classFilter = new MethodVisitorBuildConfig()

                List<String> include = map.get("include")
                String className = map.get("className")
                String interfaceName = map.get("interfaceName")
                String methodName = map.get("methodName")
                String methodDesc = map.get("methodDesc")
                Closure methodVisitor = map.get("methodVisitor")

                System.out.println("methodName == " + methodName+";methodDesc == "+methodDesc);

                AppClassModifier.include.addAll(include)
                classFilter.setInclude(include)

                classFilter.setClassName(className)
                classFilter.setInterfaceName(interfaceName)
                classFilter.setMethodName(methodName)
                classFilter.setMethodDesc(methodDesc)
                classFilter.setMethodVisitor(methodVisitor)
                configArrayList.add(classFilter)
        }
        AppClassModifier.methodVisitorList.addAll(configArrayList);
    }

}