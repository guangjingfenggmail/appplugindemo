package com.open.plugin1


import com.android.build.api.transform.Context
import com.android.build.api.transform.DirectoryInput
import com.android.build.api.transform.JarInput
import com.open.plugin1.config.CustomExtension1
import groovy.io.FileType
import org.apache.commons.io.FileUtils
import org.gradle.api.Project
import com.android.build.gradle.internal.pipeline.TransformManager
import com.android.build.api.transform.*

class AppTransform extends Transform {

    AppTransform(Project project, CustomExtension1 extension) {

    }

    @Override
    String getName() {
        return "AppTransform1"
    }

    @Override
    boolean isIncremental() {
        return false
    }


    /**
     * 需要处理的数据类型，有两种枚举类型
     * CLASSES 代表处理的 java 的 class 文件，RESOURCES 代表要处理 java 的资源
     * @return
     */
    @Override
    Set<QualifiedContent.ContentType> getInputTypes() {
        return TransformManager.CONTENT_CLASS
    }

    /**
     * 指 Transform 要操作内容的范围，官方文档 Scope 有 7 种类型：
     * 1. EXTERNAL_LIBRARIES        只有外部库
     * 2. PROJECT                   只有项目内容
     * 3. PROJECT_LOCAL_DEPS        只有项目的本地依赖(本地jar)
     * 4. PROVIDED_ONLY             只提供本地或远程依赖项
     * 5. SUB_PROJECTS              只有子项目。
     * 6. SUB_PROJECTS_LOCAL_DEPS   只有子项目的本地依赖项(本地jar)。
     * 7. TESTED_CODE               由当前变量(包括依赖项)测试的代码
     * @return
     */
    @Override
    Set<QualifiedContent.Scope> getScopes() {
        return TransformManager.SCOPE_FULL_PROJECT
    }

    @Override
    void transform(TransformInvocation transformInvocation) throws TransformException, InterruptedException, IOException {
        long startTime = System.currentTimeMillis();
        System.out.println("------------ AppPlugin start--------->startTime= " + startTime);
        transformClass(transformInvocation.context, transformInvocation.inputs, transformInvocation.outputProvider, transformInvocation.incremental)
        long endTime = System.currentTimeMillis();
        System.out.println("------------ AppPlugin end--------->endTime= " + (endTime - startTime) + " ms");
    }

    void transformClass(Context context, Collection<TransformInput> inputs, TransformOutputProvider outputProvider, boolean isIncremental)
            throws IOException, TransformException, InterruptedException {
        if (!incremental) {
            outputProvider.deleteAll()
        }
        for (TransformInput input : inputs) {
            for (DirectoryInput directoryInput : input.getDirectoryInputs()) {
                //处理文件
                processDirectoryInputWithIncremental(context, directoryInput, outputProvider, isIncremental);
            }

            for (JarInput jarInput : input.getJarInputs()) {
                //处理Jar
                processJarInputWithIncremental(context, jarInput, outputProvider, isIncremental);
            }
        }
    }


    void processDirectoryInputWithIncremental(Context context, DirectoryInput directoryInput, TransformOutputProvider outputProvider, boolean isIncremental) throws IOException {
        File dest = outputProvider.getContentLocation(
                directoryInput.getFile().getAbsolutePath(),
                directoryInput.getContentTypes(),
                directoryInput.getScopes(),
                Format.DIRECTORY);
        if (isIncremental) {
            //处理增量编译
            processDirectoryInputWhenIncremental(context, directoryInput, dest);
        } else {
            processDirectoryInput(context, directoryInput, dest);
        }
    }

    void processDirectoryInputWhenIncremental(Context context, DirectoryInput directoryInput, File dest) throws IOException {
        FileUtils.forceMkdir(dest);
        String srcDirPath = directoryInput.getFile().getAbsolutePath();
        String destDirPath = dest.getAbsolutePath();
        Map<File, Status> fileStatusMap = directoryInput.getChangedFiles();
        for (Map.Entry<File, Status> entry : fileStatusMap.entrySet()) {
            File inputFile = entry.getKey();
            Status status = entry.getValue();
            String destFilePath = inputFile.getAbsolutePath().replace(srcDirPath, destDirPath);
            File destFile = new File(destFilePath);
            if (Status.REMOVED == status) {
                if (destFile.exists()) {
                    FileUtils.forceDelete(destFile);
                }
            } else if (Status.ADDED == status || Status.CHANGED == status) {
                FileUtils.touch(destFile);
                transformSingleFile(context, inputFile, destFile, srcDirPath);
            }
        }
    }

    void processDirectoryInput(Context context, DirectoryInput directoryInput, File dest) throws IOException {
        transformDirectoryInput(context, directoryInput, dest);
    }

    void processJarInputWithIncremental(Context context, JarInput jarInput, TransformOutputProvider outputProvider, boolean isIncremental) throws IOException {
        File dest = outputProvider.getContentLocation(
                jarInput.getFile().getAbsolutePath(),
                jarInput.getContentTypes(),
                jarInput.getScopes(),
                Format.JAR);
        if (isIncremental) {
            //处理增量编译
            processJarInputWhenIncremental(context, jarInput, dest);
        } else {
            //不处理增量编译
            processJarInput(context, jarInput, dest);
        }
    }


    void processJarInput(Context context, JarInput jarInput, File dest) throws IOException {
        transformJarInput(context, jarInput, dest);
    }

    void processJarInputWhenIncremental(Context context, JarInput jarInput, File dest) throws IOException {
        if (Status.ADDED == jarInput.getStatus() || Status.CHANGED == jarInput.getStatus()) {
            //处理有变化的
            transformJarInputWhenIncremental(context, jarInput, dest, jarInput.getStatus());
        } else if (Status.REMOVED == jarInput.getStatus()) {
            //移除Removed
            if (dest.exists()) {
                FileUtils.forceDelete(dest);
            }
        }
    }

    void transformJarInputWhenIncremental(Context context, JarInput jarInput, File dest, Status status) throws IOException {
        if (status == Status.CHANGED) {
            //Changed的状态需要先删除之前的
            if (dest.exists()) {
                FileUtils.forceDelete(dest);
            }
        }
        //真正transform的地方
        transformJarInput(context, jarInput, dest);
    }

    void transformDirectoryInput(Context context, DirectoryInput directoryInput, File dest) throws IOException {
        def dir = directoryInput.getFile()
        HashMap<String, File> modifyMap = new HashMap<>()
        /**遍历以某一扩展名结尾的文件*/
        dir.traverse(type: FileType.FILES, nameFilter: ~/.*\.class/) {
            File classFile ->
                if (AppClassModifier.isShouldModify(classFile.name)) {
                    File modified = AppClassModifier.modifyClassFile(dir, classFile, context.getTemporaryDir())
                    if (modified != null) {
                        /**key 为包名 + 类名，如：/cn/sensorsdata/autotrack/android/app/MainActivity.class*/
                        String ke = classFile.absolutePath.replace(dir.absolutePath, "")
                        modifyMap.put(ke, modified)
                    }
                }
        }
        FileUtils.copyDirectory(directoryInput.file, dest)
        modifyMap.entrySet().each {
            Map.Entry<String, File> en ->
                File target = new File(dest.absolutePath + en.getKey())
                if (target.exists()) {
                    target.delete()
                }
                FileUtils.copyFile(en.getValue(), target)
                en.getValue().delete()
        }
    }

    void transformSingleFile(Context context, File inputFile, File destFile, String srcDirPath) throws IOException {
        def modified
        if (AppClassModifier.isShouldModify(inputFile.name)) {
            modified = AppClassModifier.modifyClassFile(dir, inputFile, context.getTemporaryDir())
        }
        if (modified == null) {
            modified = inputFile
        }
        FileUtils.copyFile(modified, destFile);
    }

    void transformJarInput(Context context, JarInput jarInput, File dest) throws IOException {
        def modifiedJar
        modifiedJar = AppClassModifier.modifyJar(jarInput.file, context.getTemporaryDir(), true)
        if (modifiedJar == null) {
            modifiedJar = jarInput.file
        }
        //将修改过的字节码copy到dest，就可以实现编译期间干预字节码的目的了
        FileUtils.copyFile(modifiedJar, dest)
    }
}